import React from 'react';
import Card from 'react-bootstrap/Card'
// import TotalAmount from './components/TotalAmount'

export default function Profile() {



	return (
		<Card>
		  <Card.Header className="text-center">Quote</Card.Header>
		  <Card.Body>
		    <blockquote className="blockquote mb-0" className="text-center">
		      <p>
		        {' '}
		        Becoming rich is hard. Staying broke is hard. Choose your hard.{' '}
		      </p>
		      <footer className="blockquote-footer">
		        Eric Worre
		      </footer>
		    </blockquote>
		  </Card.Body>


		</Card>
	)
}