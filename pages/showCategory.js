import React, { useState, useEffect } from 'react';
import { Table,Card } from 'react-bootstrap';

export default function ShowCategories(){

		const [categories, setCategories] = useState([]);

		// Get the user's categories when the component mounts
		useEffect(() => {

			const options = {
			headers: { Authorization: `Bearer ${localStorage.getItem('token')}` }
			};
			
			fetch(`https://rocky-brook-64027.herokuapp.com/api/users/details`, options)
			.then(res => res.json())
	        .then(data => {
	        	// console.log(data)
	        	if (data._id){ // JWT validated
	        		setCategories(data.categories)
	        	} else { // JWT is invalid or non-existent
	        		setCategories([])
	        	}
	        })
		}, [])
	

	return(
		<React.Fragment>
			<Card>
			<Card.Header className="text-center text-light bg-info">Category List
			</Card.Header>
				<Table striped bordered hover responsive className="text-center">
					<thead>
						<tr>
							<th>Category Name</th>
							<th>Category Type</th>
						</tr>
					</thead>
					<tbody>
						{categories.reverse().map(category => {
							// console.log(category)
							return(	
								<tr key={category._id}>
									<td>{category.name}</td>
									<td>{category.type}</td>
								</tr>
							)
						})}
					</tbody>
				</Table>
			</Card>
		</React.Fragment>
	)
}