import React, { useState, useEffect, useContext } from 'react';
import Router from 'next/router';
import Head from 'next/head';
import Swal from 'sweetalert2';
import { GoogleLogin } from 'react-google-login';
import { Form, Button, Card } from 'react-bootstrap';
import UserContext from '../UserContext';
import AppHelper from '../app-helper';


export default function Login() {

    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);

    function authenticate(e) {
        
        //prevent redirection via form submission
        e.preventDefault();


        fetch(`https://rocky-brook-64027.herokuapp.com/api/users/login`, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
            .then(AppHelper.toJSON)
            .then(data => {
                if (typeof data.accessToken !== 'undefined') {
                    localStorage.setItem('token', data.accessToken)
                    retrieveUserDetails(data.accessToken)
                    Router.push('/profile')
                } else {
                    if (data.error === 'does-not-exist') {
                        Swal.fire('Authentication Failed', 'User does not exist.', 'error')
                    } else if (data.error === 'login-type-error') {
                        Swal.fire('Login Type Error', 'You may have registered through a different login procedure, try an alternative login procedure.', 'error')
                    } else if(data.error === 'incorrect-password') {
                        Swal.fire('Authentication Failed', 'Password is incorrect.', 'error');
                    }
                }

            })
    }



    const authenticateGoogleToken = (response) => {


        const payload = {

            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify({ tokenId: response.tokenId })

        }

        fetch(`http://localhost:4000/api/users/verify-google-id-token`, payload)
        .then(AppHelper.toJSON)
        .then(data => {
            if(typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
                Router.push('/profile')
            } else {
                if (data.error === 'google-auth-error') {
                    Swal.fire(
                        'Google Auth Error',
                        'Google authentication procedure failed',
                        'error'
                    )
                } else if (data.error === 'login-type-error') {
                    Swal.fire(
                        'Login Type Error',
                        'You may have registered through a different login procredure.',
                        'error'
                    )
                }
            }
        })
    }

    const retrieveUserDetails = (accessToken) => {

        const options = {
            headers: { Authorization: `Bearer ${accessToken}` }
        };

        // localhost:4000/api/users/details
        fetch(`http://localhost:4000/api/users/details`, options)
            .then(AppHelper.toJSON)
            .then(data => {

                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                });
            })

    }

   useEffect(() => {
        if ((email !== '' && password !== '')) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [email, password]) 

   return (
        <Card>
            <Card.Header>Login Details</Card.Header>
            <Card.Body>
                <Form onSubmit={authenticate}>
                    <Form.Group controlId="userEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control
                            type="email"
                            placeholder="Enter email"
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            autoComplete="off"
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                            type="password"
                            placeholder="Password"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            required
                        />
                    </Form.Group>

                    {isActive
                        ?
                        <Button className="bg-primary" type="submit">
                            Submit
                        </Button>
                        :
                        <Button className="bg-danger" type="submit" disabled>
                            Submit
                        </Button>
                    }


                    <GoogleLogin
                        clientId="469910252554-5te1jmfsnkc2t6frkrmrub78c2qdvgqj.apps.googleusercontent.com"
                        buttonText="Login"
                        onSuccess={authenticateGoogleToken}
                        onFailure={authenticateGoogleToken}
                        cookiePolicy={'single_host_origin'}
                        className="w-100 text-center d-flex justify-content-center"
                    />

                        </Form>
            </Card.Body>
        </Card>
    )
}
