import { useContext, useEffect } from 'react';
import Router from 'next/router';
import UserContext from '../UserContext';

export default function index() {
	const { unsetUser, setUser } = useContext(UserContext);

	// invoke unsetUser() to clear the local storage
	useEffect(() => {
		unsetUser();
		Router.push('/login')
	}, [])

	return null
}